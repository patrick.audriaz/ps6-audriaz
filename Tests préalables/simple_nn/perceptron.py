import numpy as np

# sigmoid activation and normalization function
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

#compute derivative to find error rate and adjust weights
def sigmoid_derivative(x):
    return x * (1 - x)


# input data matrix, 3 row (inputs) and 4 columns (data)
training_inputs = np.array([[0, 0, 1],
                            [1, 1, 1],
                            [1, 0, 1],
                            [0, 1, 1]])

# output/result data matrix, should be "1" if first number of input data is "1", transposed to match with training_inputs
training_outputs = np.array([[0, 1, 1, 0]]).T

# initialize random weights between -1 and 1 in a 3*1 matrix
weights = 2 * np.random.rand(3, 1) - 1

print("Initial weights : \n", weights)

# training
for iteration in range(10000):
    # define input layer
    input_layer = training_inputs

    # normalize the product of the input layer and the weight, sent to sigmoid function (forward propagation)
    outputs = sigmoid(np.dot(input_layer, weights))

    #back propagation, use loss function to calculate error value
    error = training_outputs - outputs

    #gradient descent or minimizing, must bo closest to 0
    adjustements = error * sigmoid_derivative(outputs)

    # update weights
    weights += np.dot(input_layer.T, adjustements)

print("Final weights : \n", weights)
print("Training result : \n", outputs)
