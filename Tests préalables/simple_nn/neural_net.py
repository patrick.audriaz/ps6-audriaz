# class of perceptron.py

import numpy as np


class NeuralNetwork():

    def __init__(self):
        # initialize random weights between -1 and 1 in a 3*1 matrix
        self.weights = 2 * np.random.rand(3, 1) - 1

    # sigmoid activation and normalization function
    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    # compute derivative to find error rate
    def sigmoid_derivative(self, x):
        return x * (1 - x)

    def train(self, training_inputs, training_outputs, training_iteration):
        # training
        for iteration in range(training_iteration):
            # pass training set through the neural network
            output = self.think(training_inputs)

            # back propagation, use loss function to calculate error value
            error = training_outputs - output

            # multiply error by input and gradient of the sigmoid function, less confident weights are adjusted more through the nature of the function
            adjustments = np.dot(training_inputs.T, error *
                                 self.sigmoid_derivative(output))

            # adjust weights
            self.weights += adjustments

    def think(self, inputs):
        #  pass inputs through the neural network to get output
        inputs = inputs.astype(float)
        output = self.sigmoid(np.dot(inputs, self.weights))

        return output


if __name__ == "__main__":

    # Initialize the single neuron neural network
    nn = NeuralNetwork()

    print("Random weights: \n", nn.weights)

    # The training set, with 4 examples consisting of 3 input values and 1 output value
    training_inputs = np.array([[0, 0, 1],
                                [1, 1, 1],
                                [1, 0, 1],
                                [0, 1, 1]])

    training_outputs = np.array([[0, 1, 1, 0]]).T

    # Train the neural network
    nn.train(training_inputs, training_outputs, 10000)

    print("Weights after training: \n", nn.weights)

    A = str(input("Input 1: "))
    B = str(input("Input 2: "))
    C = str(input("Input 3: "))

    print("New situation: input data = ", A, B, C)
    print("Output data: \n", nn.think(np.array([A, B, C])))
