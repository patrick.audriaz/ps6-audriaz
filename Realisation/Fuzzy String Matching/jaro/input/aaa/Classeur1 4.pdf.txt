Monsieur Patient MONDEMO, né le 22.01.1 956, Page 2

•
•
•
•

Status après AIT en 2005.
Cures d'hémorroïdes en 1960 et 1980.
Status après résection transurétrale de la prostate en 1999.
Malaria en 1948, avec récidive 15 et 30 ans plus tard.

ANAMNESE ACTUELLE

Le patient souffre d'une sténose bulbaire secondaire à de multiples résections transurétrales de
la prostate. L'indication à une urétrotomie interne est posée, raison de l'hospitalisation actuelle.
EXAMEN CLINIQUE D'ENTREE

Patient en bon état général, apyrétique, anictérique, bien hydraté et bien perfusé. Auscultation
cardio-pulmonaire physiologique. Abdomen souple et indolore. Le status urologique complet a
été fait par le Dr Urologue.
Patient orienté dans le temps et l'espace, Glasgow 15. Pas de méningisme, ni de latéralisation.
DISCUSSION ET EVOLUTION:

L'intervention se déroule sans complication, les suites opératoires sont simples, le patient reste
apyrétique. La sonde vésicale est retirée à 48 heures, avec reprise des mictions sans
particularité.
L'antalgie étant bien réglée et, au vu de la bonne évolution, Monsieur MONDEMO peut regagner
son domicile le 13.05.201O. Il sera revu à la consultation du Dr Urologue pour contrôle dans un
mois.
TRAITEMENT DE SORTIE

Ciprofloxacine 500 mg, 2 x 1 cpr/jour pendant 5 jours.
Dafalgan 4 x 1 g/jour.
Halcion 0,25 mg en réserve.
Nozinan 25 mg, 1 cpr le soir.
Concor 2,5 mg, ½ cpr le matin.
Sifrol 0, 125 mg, 1 à 2 cpr au coucher.

Nous sommes à votre disposition pour tout renseignement complémentaire et vous adressons,
Monsi.e ur etI'cher Confrère, nos salutations les meilleures.

Copie : Dr Urologue

