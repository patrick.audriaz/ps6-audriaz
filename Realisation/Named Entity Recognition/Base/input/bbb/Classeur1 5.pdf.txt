tél 026 347 45 00
fax 026 347 45 05

promed SA
Ancienne Papeterie 131
Case postale 224
1723 Marly 1

promed
l a b o r a t o i r e mé d i c a l

Demande n°

22.03-1265

Rapport
Prélevé le
Clôturé le
Validé par
Facturé

FINAL
07.09.10 à 15:41
13.09.10

du 08.09.10 à

www.promed-lab.ch
info @promed-lab.ch

Marly, le 13.09.10

12:36

Docteur
Test MEDECIN
rte. d'Englisberg 13
1763 Granges-Paccot

Au patient

Copie :

MONDEMO Patient, 22.01.1956 (M), RUE DE LA CLINIQUE 6, 1763 Granges-Paccot

RAPPORT BACTERIOLOGIQUE
PRELEVEMENT
Culture aérobie

Uricult
Staphylococcus epidermidis

ANTIBIOGRAMME
Amoxicilline
Oxacilline - Flucloxacilline
Amoxicilline/Ac.clavul.
Cefuroxime
Cefuroxime axetil
Ciprofloxacine - Ofloxacine

Germe n°

10e6

Réf. n° 1

1
R
R
R
R
R
R

R = Résistant
S = Sensible

1
Lévofloxacine
Trimethoprim/Sulfamethoxazole
Fosfomycine
Nitrofurantoine - Furadantine
Linezolid
Doxycycline

R
S
S
S
S
S

I = Intermédiaire
N = Non testé

Les rapports en annexe vous sont transmis par courrier séparé.
Ce rapport a été validé électroniquement.
Avec nos meilleurs remerciements - Imprimé le 13.09.10
à 17:57

Page
Rapport

1/1
1*

