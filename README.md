# Medical Machine Learning

Machine learning pour l'extraction d'informations et la classification de dossier médicaux.

## Contexte
Fondée en 2009 sous l'impulsion d'un informaticien et de deux médecins, e-sculape est une société spécialisée en informatique médicale concentrant son activité dans le domaine de la médecine ambulatoire.

En phase de diversification et à la recherche de solution pour permettre au cabinet de faciliter leur travail de numérisation, nous avons imaginé une application de scanning de dossiers mé-dicaux papier.

Les besoins liés au standard eHealth de la confédération suisse, détermine les besoins dans les cabinets. A l'heure où encore de nombreux dossiers médicaux sont à l'état papier, il est important d'avoir une application rapide et efficace pour passer au tout numérique.

Cette application se greffera sur une application déjà développée et permettra un apprentissage du contenu afin de le mettre à disposition du professionnel de la santé des do-cuments dont la classification et la fusion se fera sur la base de l'auto-apprentissage.

La première phase du projet a été basée sur une reconnaissance des types de documents par QRCode.

## Objectifs

*	Analyse du marché pour une application de scanning dans les cabinets médicaux
*	Développement d'une application de type machine Learning pour détecter les types de documents ainsi que son auteur, le destinataire, le type de document, la date et le pa-tient.
*	Garantir un scanning de masse avec une classification automatique
*	Etude du meilleure modèle économique
*	En faire un produit commercialisable

## Auteur

* **Patrick Audriaz**

## Superviseurs

* **Andreas Fischer**
* **Nicolas Schroeter**

## Mandant

* **J. Clément (e-sculape)**